//
//  PrimedIO.m
//  PrimedIO
//
//  Created by PrimedIO on 6/1/18.
//  Copyright © 2018 PrimedIO. All rights reserved.
//

#import "Primed.h"
#import <CommonCrypto/CommonCrypto.h>
#import "PrimedTracker.h"

@implementation Primed

#pragma mark - HTTP Methods
-(NSMutableURLRequest *)requestWithURL:(NSString *)urlLink method:(NSString *)method{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",Primed.sharedInstance.connectionStringPrimedIO,urlLink];

    NSURL *URL = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:Primed.sharedInstance.public_key forHTTPHeaderField:@"X-Authorization-Key"];
    [request addValue:Primed.sharedInstance.sha512Signature forHTTPHeaderField:@"X-Authorization-Signature"];
    [request addValue:Primed.sharedInstance.nonce forHTTPHeaderField:@"X-Authorization-Nonce"];
    [request setHTTPMethod:method];
    
    return request;
}
-(void)sessionRequest:(NSURLRequest *)request success:(APISuccess)success failed:(APIFailed)failed{
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if([[NSThread currentThread] isMainThread]){
            if(error){
                if(Primed.sharedInstance.debug){ PrimedLog(@"[ERROR] %@: %@",request.URL,error.userInfo); }
                failed(error.userInfo[@"NSLocalizedDescription"]);
            }
            else{
                NSDictionary *dictReturn = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                if(dictReturn){
                    if(Primed.sharedInstance.debug){ PrimedLog(@"[SUCCESS] %@: %@",request.URL,dictReturn); }
                    success(dictReturn);
                }
                else{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if(httpResponse.statusCode >= 200 && httpResponse.statusCode <= 210){
                        if(Primed.sharedInstance.debug){ PrimedLog(@"[SUCCESS] %@",request.URL); }
                        success(dictReturn);
                    }
                    else{
                        if(Primed.sharedInstance.debug){ PrimedLog(@"[FAILED] %@ Error: %@",request.URL,[NSString.alloc initWithData:data encoding:NSUTF8StringEncoding]); }
                        failed([NSString stringWithFormat:@"Error CODE: %ld",(long)httpResponse.statusCode]);
                    }
                }
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    if(Primed.sharedInstance.debug){ PrimedLog(@"[ERROR] %@: %@",request.URL,error.userInfo); }
                    failed(error.userInfo[@"NSLocalizedDescription"]);
                }
                else{
                    NSDictionary *dictReturn = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    if(dictReturn){
                        if(Primed.sharedInstance.debug){ PrimedLog(@"[SUCCESS] %@: %@",request.URL,dictReturn); }
                        success(dictReturn);
                    }
                    else{
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if(httpResponse.statusCode >= 200 && httpResponse.statusCode <= 210){
                            if(Primed.sharedInstance.debug){ PrimedLog(@"[SUCCESS] %@",request.URL); }
                            success(dictReturn);
                        }
                        else{
                            if(Primed.sharedInstance.debug){ PrimedLog(@"[FAILED] %@ Error: %@",request.URL,[NSString.alloc initWithData:data encoding:NSUTF8StringEncoding]); }
                            failed([NSString stringWithFormat:@"Error CODE: %ld",(long)httpResponse.statusCode]);
                        }
                    }
                }
            });
        }
    }] resume];
}
-(void)post:(NSDictionary *)param onURL:(NSString *)urlPath success:(APISuccess)success failed:(APIFailed)failed{
    NSMutableURLRequest *request = [self requestWithURL:urlPath method:@"POST"];
    
    if(param){
        NSError *jsonError;
        NSData *sendData = [NSJSONSerialization dataWithJSONObject:param options:0 error:&jsonError];
        if(jsonError){
            failed(@"Params not valid JSON format");
            return;
        }
        [request setHTTPBody:sendData];
    }
    
    [self sessionRequest:request success:success failed:failed];
}
-(void)get:(NSString *)urlParam success:(APISuccess)success failed:(APIFailed)failed{
    NSMutableURLRequest *request = [self requestWithURL:urlParam method:@"GET"];
    [self sessionRequest:request success:success failed:failed];
}

#pragma mark - CustomFunctions
+(NSString *)hmac:(NSString *)string{
    const char *cstr = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:string.length];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;
}

#pragma mark - Initialization
+(Primed *)sharedInstance{
    static dispatch_once_t pred;
    static Primed *sharedBridge = nil;
    dispatch_once(&pred, ^{
        sharedBridge = [[self alloc] init];
    });
    return sharedBridge;
}
-(id)init{
    self = [super init];
    if(self) {
        
    }
    return self;
}
-(void)initWithPublicKey:(NSString *)publicKey secretKey:(NSString *)secretKey connectionString:(NSString *)connectionString{
    if(Primed.sharedInstance.debug){
        PrimedLog(@"Primed Debug Activated");
    }
    
    NSLog(@"Primed.sharedInstance.public_key: %@",Primed.sharedInstance.public_key);
    if(Primed.sharedInstance.public_key){
        NSLog(@"[PRIMED] Please use only one initialisation");
        return;
    }
    
    Primed.sharedInstance.public_key = publicKey;
    Primed.sharedInstance.nonce = [NSString stringWithFormat:@"%.f",NSDate.date.timeIntervalSince1970];
    NSString *prepSignature = [NSString stringWithFormat:@"%@%@%@",publicKey,secretKey,self.nonce];
    Primed.sharedInstance.sha512Signature = [Primed hmac:prepSignature];
    Primed.sharedInstance.connectionStringPrimedIO = connectionString;
}

#pragma mark - API Main Functions
-(void)personalise:(NSString *)campaign limit:(NSUInteger)limit abvariantLabel:(NSString *)abvariantLabel success:(APISuccess)success failed:(APIFailed)failed{
    [self personalise:campaign signals:@{} limit:limit abvariantLabel:abvariantLabel success:success failed:failed];
}
-(void)personalise:(NSString *)campaign signals:(NSDictionary<NSString*,id> *)signals limit:(NSUInteger)limit abvariantLabel:(NSString *)abvariantLabel success:(APISuccess)success failed:(APIFailed)failed{
    
    NSMutableDictionary *trackSignals = [NSMutableDictionary dictionaryWithDictionary:signals];
    if(![trackSignals.allKeys containsObject:@"did"]){
        if(Primed.sharedInstance.primedTrackerAvailable){
            [trackSignals setObject:PrimedTracker.sharedInstance.getDID forKey:@"did"];
        }
    }
    if(![trackSignals.allKeys containsObject:@"sid"]){
        if(Primed.sharedInstance.primedTrackerAvailable){
            [trackSignals setObject:PrimedTracker.sharedInstance.getSID forKey:@"sid"];
        }
    }
    
    NSError *errorJSON;
    NSData *signalsJSON = [NSJSONSerialization dataWithJSONObject:trackSignals options:NSJSONWritingPrettyPrinted error:&errorJSON];
    if(errorJSON){
        failed(@"Signals not valid format");
        return;
    }
    NSString *signalToString = [[NSString alloc] initWithData:signalsJSON encoding:NSUTF8StringEncoding];
    NSCharacterSet *setEncoding = [NSCharacterSet URLHostAllowedCharacterSet];
    
    NSMutableString *generateURL = [NSMutableString stringWithString:@"/api/v1/personalise?"];
    [generateURL appendFormat:@"campaign=%@",campaign];
    [generateURL appendFormat:@"&limit=%lu",(unsigned long)limit];
    [generateURL appendFormat:@"&abvariant=%@",[abvariantLabel stringByAddingPercentEncodingWithAllowedCharacters:setEncoding]];
    [generateURL appendFormat:@"&signals=%@",[signalToString stringByAddingPercentEncodingWithAllowedCharacters:setEncoding]];
    
    [self get:generateURL success:^(NSDictionary *response) {
        
        if(Primed.sharedInstance.primedTrackerAvailable){
            [PrimedTracker.sharedInstance trackPersonalise:@{@"guuid":response[@"guuid"]}];
        }
        
        success(response);
    } failed:^(NSString *message) {
        failed(message);
    }];
}
-(void)convert:(NSString *)ruuid{
    [self convert:ruuid data:nil];
}
-(void)convert:(NSString *)ruuid data:(NSDictionary<NSString*,id> *)data{
    NSString *strUrl = [NSString stringWithFormat:@"/api/v1/conversion/%@",ruuid];
    [self post:data onURL:strUrl success:^(NSDictionary *response) {
        
        if(Primed.sharedInstance.primedTrackerAvailable){
            [PrimedTracker.sharedInstance trackConvert:@{@"ruuid":ruuid,
                                                         @"data":data
                                                         }];
        }
        
    } failed:^(NSString *message) {
        
    }];
}
-(void)health:(APISuccess)success failed:(APIFailed)failed{
    [self get:@"/api/v1/health" success:^(NSDictionary *response) {
        success(response);
    } failed:^(NSString *message) {
        failed(message);
    }];
}

@end
