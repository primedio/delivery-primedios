//
//  PrimedIO.h
//  PrimedIO
//
//  Created by PrimedIO on 6/1/18.
//  Copyright © 2018 PrimedIO. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PrimedLog(FORMAT, ...) fprintf(stderr,"[%s][Line %d]%s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

typedef void(^APISuccess)(NSDictionary<NSString*,id> *response);
typedef void(^APIFailed)(NSString *message);

@interface Primed : NSObject

@property (nonatomic,assign) BOOL debug;
@property (nonatomic,strong) NSString *sha512Signature;
@property (nonatomic,strong) NSString *public_key;
@property (nonatomic,strong) NSString *nonce;
@property (nonatomic,strong) NSString *connectionStringPrimedIO;
@property (nonatomic,assign) BOOL primedTrackerAvailable;

/**
 * Create instance of Primed class
 * @return Primed shared instance
 */
+(Primed *)sharedInstance;

/**
 * Create instance of Primed class
 * @param publicKey Public Key provided by PrimedIO
 * @param secretKey Secret Key provided by PrimedIO
 * @param connectionString URL to API for PrimedIO service
 */
-(void)initWithPublicKey:(NSString *)publicKey secretKey:(NSString *)secretKey connectionString:(NSString *)connectionString;

/**
 * Deal with the results as they are returned
 * @param campaign Campaign to personalise
 * @param limit Limit the response
 * @param abvariantLabel Abvariant Label
 */
-(void)personalise:(NSString *)campaign limit:(NSUInteger)limit abvariantLabel:(NSString *)abvariantLabel success:(APISuccess)success failed:(APIFailed)failed;

/**
 * Deal with the results as they are returned
 * @param campaign Campaign to personalise
 * @param signals Dictionary signals
 * @param limit Limit the response
 * @param abvariantLabel Abvariant Label
 */
-(void)personalise:(NSString *)campaign signals:(NSDictionary<NSString*,id> *)signals limit:(NSUInteger)limit abvariantLabel:(NSString *)abvariantLabel success:(APISuccess)success failed:(APIFailed)failed;

/**
 * Converting clicks.
 * Each result comes with a guaranteed random UUID. This UUID can be sent back to
 * PrimedIO to mark a result as having been converted. This in turn allows the system
 * to evaluate the performance (as CTR) of the underlying Model(blend).
 * @param ruuid Random UUID
 */
-(void)convert:(NSString *)ruuid;

/**
 * Converting clicks.
 * Each result comes with a guaranteed random UUID. This UUID can be sent back to
 * PrimedIO to mark a result as having been converted. This in turn allows the system
 * to evaluate the performance (as CTR) of the underlying Model(blend).
 * @param ruuid Random UUID
 * @param data Additional data to convert
 */
-(void)convert:(NSString *)ruuid data:(NSDictionary<NSString*,id> *)data;

/**
 * Check Health of system.
 */
-(void)health:(APISuccess)success failed:(APIFailed)failed;

+(NSString *)hmac:(NSString *)string;

@end
