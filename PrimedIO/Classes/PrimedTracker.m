//
//  PrimedTracker.m
//  PrimedIO
//
//  Created by PrimedIO on 7/23/18.
//  Copyright © 2018 PrimedIO. All rights reserved.
//

#import "PrimedTracker.h"
#import "Primed.h"

@import AdSupport;

@implementation PrimedTracker

#pragma mark - SimpleDatabase
-(NSString *)pathPayloads{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths.firstObject;
    return [documentsDirectory stringByAppendingPathComponent:@"Payloads.plist"];
}
-(void)saveToPLIST:(NSDictionary<NSString*,id> *)payload{
    [savedPayloads addObject:payload];
    [savedPayloads writeToFile:self.pathPayloads atomically:YES];
}
-(void)removeFirstItemFromPLIST{
    [savedPayloads removeObjectAtIndex:0];
    [savedPayloads writeToFile:self.pathPayloads atomically:YES];
}

#pragma mark - CustomFunctions
-(NSString *)getUA{
    return [NSString stringWithFormat:@"%@;%@",UIDevice.currentDevice.model,UIDevice.currentDevice.systemVersion];
}
-(NSString *)getDate_iso8601{
    NSDateFormatter *dateFormatter = NSDateFormatter.new;
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDate *now = [NSDate date];
    NSString *iso8601String = [dateFormatter stringFromDate:now];
    return iso8601String;
}
-(BOOL)isNetworkAvailable{
    CFNetDiagnosticRef dReference;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnonnull"
    dReference = CFNetDiagnosticCreateWithURL (NULL, (__bridge CFURLRef)[NSURL URLWithString:@"www.apple.com"]);
#pragma clang diagnostic pop

    CFNetDiagnosticStatus status;
    status = CFNetDiagnosticCopyNetworkStatusPassively (dReference, NULL);
    CFRelease (dReference);
    if ( status == kCFNetDiagnosticConnectionUp ){
        return YES;
    }
    else{
        return NO;
    }
}
-(NSDictionary *)basicEventProperties:(NSString *)type eventObject:(NSDictionary<NSString*,id> *)eventObject{
    if(!Primed.sharedInstance.public_key){
        NSLog(@"[PRIMED] Tracker not initialized - missing public key");
    }
    NSMutableDictionary *payload = [NSMutableDictionary
                                    dictionaryWithDictionary:@{@"apikey":(Primed.sharedInstance.public_key ?: @""),
                                                               @"ts":@((NSUInteger)(NSDate.date.timeIntervalSince1970 * 1000)),
                                                               @"sid":sid,
                                                               @"did":did,
                                                               @"source":@"APP",
                                                               @"sdkId":@(2),
                                                               @"sdkVersion":@"0.0.6",
                                                               @"type":type.uppercaseString,
                                                               @"eventObject":(eventObject ?: @{})
                                                               }];
    if(self.customBasicProperties){
        [payload addEntriesFromDictionary:@{@"customProperties":self.customBasicProperties}];
    }
    if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"[basicEventProperties] %@",payload); }
    return payload;
}
-(void)emitPayload:(NSDictionary<NSString*,id> *)payload{
    
    if(self.isNetworkAvailable && self->isConnected){
//        [[self.socket emit:@"event" with:@[payload]]];
        [[self.socket emitWithAck:@"event" with:@[payload]] timingOutAfter:0 callback:^(NSArray* data) {
            if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"data return: %@",data); }
        }];
    }
    else{
        [self saveToPLIST:payload];
    }
}
-(void)emitFromDatabase{
    if(self->savedPayloads.count > 0 && self.isNetworkAvailable && self->isConnected){
        if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"Left to send: %lu",(unsigned long)self->savedPayloads.count); }
        [self emitPayload:self->savedPayloads.firstObject];
        [self removeFirstItemFromPLIST];
    }
}

#pragma mark - Initialization
+(PrimedTracker *)sharedInstance{
    static dispatch_once_t pred;
    static PrimedTracker *sharedBridge = nil;
    dispatch_once(&pred, ^{
        sharedBridge = [[self alloc] init];
    });
    return sharedBridge;
}
-(id)init{
    self = [super init];
    if(self) {
        savedPayloads = [NSMutableArray arrayWithContentsOfFile:self.pathPayloads];
        if(!savedPayloads){
            savedPayloads = [NSMutableArray new];
        }
        
        sid = [[NSUUID UUID] UUIDString];
        did = UIDevice.currentDevice.identifierForVendor.UUIDString;
    }
    
    return self;
}
-(void)initWithPublicKey:(NSString *)publicKey secretKey:(NSString *)secretKey connectionString:(NSString *)connectionString trackingConnectionString:(NSString *)trackingConnectionString heartbeatInterval:(NSInteger)heartbeatInterval eventQueueRetryInterval:(NSInteger)eventQueueRetryInterval{
    
    if(PrimedTracker.sharedInstance.debug){
        PrimedLog(@"Primed Tracker Debug Activated");
    }
    
    if(Primed.sharedInstance.public_key){
        NSLog(@"[PRIMED] Please use only one initialisation");
        return;
    }
    
    Primed.sharedInstance.public_key = publicKey;
    Primed.sharedInstance.nonce = [NSString stringWithFormat:@"%.f",NSDate.date.timeIntervalSince1970];
    NSString *prepSignature = [NSString stringWithFormat:@"%@%@%@",Primed.sharedInstance.public_key,secretKey,Primed.sharedInstance.nonce];
    Primed.sharedInstance.sha512Signature = [Primed hmac:prepSignature];
    Primed.sharedInstance.connectionStringPrimedIO = connectionString;
    Primed.sharedInstance.primedTrackerAvailable = YES;
    self->heartbeatIncrement = 0;
    self->isConnected = false;
    
    self.heartbeatInterval = heartbeatInterval;
    self.eventQueueRetryInterval = eventQueueRetryInterval;
    self.trackingConnectionString = trackingConnectionString;
    
    NSURL *url = [NSURL URLWithString:self.trackingConnectionString];
    self.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"forceWebsockets":@YES,
                                                                         @"log": @(PrimedTracker.sharedInstance.debug)}];
    self.socket = [[SocketIOClient alloc] initWithManager:self.manager nsp:@"/v1"];
    [self.manager setNsps:@{@"/v1":self.socket}];
    
    [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"[PRIMED TRACKER SOCKET CONNECTED]"); }
        self->isConnected = true;
    }];
    
    [self.socket on:@"disconnect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"[PRIMED TRACKER SOCKET DISCONNECTED]"); }
        self->isConnected = false;
    }];
    
    [self.socket onAny:^(SocketAnyEvent *data) {
        if(PrimedTracker.sharedInstance.debug){ PrimedLog(@"onAny: [%@] %@",data.event,data.items); }
    }];
    
    [self.socket connect];
    
    __block NSTimer *heartBeatTimer;
    __block NSTimer *eventQueueTimer;

    [NSNotificationCenter.defaultCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        
        if (!heartBeatTimer) {
            if (@available(iOS 10.0, *)) {
                heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:self.heartbeatInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
                    [self trackHeartbeat];
                }];
            } else {
                heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:self.heartbeatInterval target:self selector:@selector(trackHeartbeat) userInfo:nil repeats:YES];
            }
        }
        
        if (!eventQueueTimer) {
            if (@available(iOS 10.0, *)) {
                eventQueueTimer = [NSTimer scheduledTimerWithTimeInterval:self.eventQueueRetryInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
                    [self emitFromDatabase];
                }];
            } else {
                eventQueueTimer = [NSTimer scheduledTimerWithTimeInterval:self.eventQueueRetryInterval target:self selector:@selector(emitFromDatabase) userInfo:nil repeats:YES];
            }
        }
        
        [self trackStart:@""];
    }];

    [NSNotificationCenter.defaultCenter addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        
        if ([heartBeatTimer isValid]) {
            [heartBeatTimer invalidate];
        }
        heartBeatTimer = nil;
        
        if ([eventQueueTimer isValid]) {
            [eventQueueTimer invalidate];
        }
        eventQueueTimer = nil;
        
        
        // reset the heartbeat increment counter so the next 'session' starts at `i: 0`
        self->heartbeatIncrement = 0;
        
        [self trackEnd];
    }];
    
    if (!heartBeatTimer) {
        if (@available(iOS 10.0, *)) {
            heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:self.heartbeatInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
                [self trackHeartbeat];
            }];
        } else {
            heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:self.heartbeatInterval target:self selector:@selector(trackHeartbeat) userInfo:nil repeats:YES];
        }
    }
    
    if (!eventQueueTimer) {
        if (@available(iOS 10.0, *)) {
            eventQueueTimer = [NSTimer scheduledTimerWithTimeInterval:self.eventQueueRetryInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
                [self emitFromDatabase];
            }];
        } else {
            eventQueueTimer = [NSTimer scheduledTimerWithTimeInterval:self.eventQueueRetryInterval target:self selector:@selector(emitFromDatabase) userInfo:nil repeats:YES];
        }
    }
    
    [self trackStart:@""];
}

#pragma mark - API Main Functions
-(void)trackClick:(NSInteger)x y:(NSInteger)y{
    [self trackClick:x y:y interactionType:-1];
}
-(void)trackClick:(NSInteger)x y:(NSInteger)y interactionType:(InteractionType)interactionType{
    NSDictionary *payload = [self basicEventProperties:@"click" eventObject:@{@"x":@(x),
                                                                              @"y":@(y),
                                                                              @"interactionType": @(interactionType == 0 ?: interactionType)
                                                                              }];
    [self emitPayload:payload];
}
-(void)trackView:(NSString *)uri{
    [self trackView:uri customProperties:@{}];
}
-(void)trackView:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties{
    NSDictionary *payload = [self basicEventProperties:@"view" eventObject:@{@"uri":(uri ?: @""),
                                                                             @"customProperties":(customProperties ?: @{})
                                                                             }];
    [self emitPayload:payload];
}
-(void)trackScroll:(ScrollDirection)scrollDirection distance:(NSInteger)distance{
    NSDictionary *payload = [self basicEventProperties:@"distance" eventObject:@{@"direction":@(scrollDirection),
                                                                                 @"distance":@(distance)
                                                                                 }];
    [self emitPayload:payload];
}
-(void)trackEnterViewPort:(NSDictionary<NSString*,id> *)customProperties{
    NSDictionary *payload = [self basicEventProperties:@"enterViewPort" eventObject:@{@"customProperties":(customProperties ?: @{})}];
    [self emitPayload:payload];
}
-(void)trackExitViewPort:(NSDictionary<NSString*,id> *)customProperties{
    NSDictionary *payload = [self basicEventProperties:@"exitViewPort" eventObject:@{@"customProperties":(customProperties ?: @{})}];
    [self emitPayload:payload];
}
-(void)trackPositionChange:(CLLocation *)location{
    NSDictionary *payload = [self basicEventProperties:@"positionchange" eventObject:@{@"latitude":@(location.coordinate.latitude),
                                                                                       @"longitude":@(location.coordinate.longitude),
                                                                                       @"accuracy":@(location.horizontalAccuracy)
                                                                                       }];
    [self emitPayload:payload];
}
-(void)trackHeartbeat{
    NSDictionary *payload = [self basicEventProperties:@"heartbeat" eventObject:@{@"i":@(heartbeatIncrement)}];
    [self emitPayload:payload];
    heartbeatIncrement++;
}
-(void)trackStart:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties{
    NSDictionary *payload = [self basicEventProperties:@"start" eventObject:@{@"ua":self.getUA,
                                                                              @"now":self.getDate_iso8601,
                                                                              @"uri":(uri ?: @""),
                                                                              @"screenWidth":@(UIScreen.mainScreen.bounds.size.width),
                                                                              @"screenHeight":@(UIScreen.mainScreen.bounds.size.height),
                                                                              @"viewPortWidth":@(UIScreen.mainScreen.bounds.size.width),
                                                                              @"viewPortHeight":@(UIScreen.mainScreen.bounds.size.height),
                                                                              @"customProperties":(customProperties ?: @{})
                                                                              }];
    [self emitPayload:payload];
}
-(void)trackStart:(NSString *)uri{
    [self trackStart:uri customProperties:nil];
}
-(void)trackEnd{
    NSDictionary *payload = [self basicEventProperties:@"end" eventObject:nil];
    [self emitPayload:payload];
}
-(void)trackCustomEvent:(NSString *)eventType customProperties:(NSDictionary<NSString*,id> *)customProperties{
    NSDictionary *payload = [self basicEventProperties:(eventType ?: @"") eventObject:(customProperties ?: @{})];
    [self emitPayload:payload];
}
-(void)trackPersonalise:(NSDictionary<NSString*,id> *)data{
    NSDictionary *payload = [self basicEventProperties:@"personalise" eventObject:(data ?: @{})];
    [self emitPayload:payload];
}
-(void)trackConvert:(NSDictionary<NSString*,id> *)data{
    NSDictionary *payload = [self basicEventProperties:@"convert" eventObject:(data ?: @{})];
    [self emitPayload:payload];
}

#pragma mark - Getters
-(NSString *)getDID{
    return did;
}
-(NSString *)getSID{
    return sid;
}

@end
