//
//  PrimedTracker.h
//  PrimedIO
//
//  Created by PrimedIO on 7/23/18.
//  Copyright © 2018 PrimedIO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Primed.h"

@import CoreLocation;
@import SocketIO;

typedef NS_ENUM(NSUInteger, InteractionType) {
    InteractionType_LEFT,
    InteractionType_RIGHT,
    InteractionType_MIDDLE,
    InteractionType_OTHER,
    InteractionType_LONGPRESS,
};
typedef NS_ENUM(NSUInteger, ScrollDirection) {
    ScrollDirection_UP,
    ScrollDirection_DOWN,
    ScrollDirection_LEFT,
    ScrollDirection_RIGHT
};

@interface PrimedTracker : Primed {
    Boolean isConnected;
    NSInteger heartbeatIncrement;
    NSMutableArray *savedPayloads;
    NSString *sid;
    NSString *did;
}

@property (nonatomic,strong) NSString *trackingConnectionString;
@property (nonatomic,assign) NSInteger heartbeatInterval;
@property (nonatomic,assign) NSInteger eventQueueRetryInterval;
@property (nonatomic,strong) SocketIOClient *socket;
@property (nonatomic,strong) SocketManager *manager;
@property (nonatomic,strong) NSDictionary<NSString*,id> *customBasicProperties;

/**
 * Create instance of PrimedTracker class
 * @return PrimedTracker shared instance
 */
+(PrimedTracker *)sharedInstance;

/**
 * Create instance of PrimedTracker class
 * @param publicKey Public Key provided by PrimedIO
 * @param secretKey Secret Key provided by PrimedIO
 * @param connectionString URL to API for PrimedIO service
 * @param trackingConnectionString URL to API for Primed tracking service
 * @param heartbeatInterval need info
 * @param eventQueueRetryInterval need info
 */
-(void)initWithPublicKey:(NSString *)publicKey secretKey:(NSString *)secretKey connectionString:(NSString *)connectionString trackingConnectionString:(NSString *)trackingConnectionString heartbeatInterval:(NSInteger)heartbeatInterval eventQueueRetryInterval:(NSInteger)eventQueueRetryInterval;

/**
 * Track Clicks
 * @param x X Coordinate
 * @param y Y Coordinate
 * @param interactionType Enumeration click type
 */
-(void)trackClick:(NSInteger)x y:(NSInteger)y interactionType:(InteractionType)interactionType;
-(void)trackClick:(NSInteger)x y:(NSInteger)y;

/**
 * Track URI
 * @param uri URI Tracking
 * @param customProperties Custom NSDictionary property
 */
-(void)trackView:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties;
-(void)trackView:(NSString *)uri;

/**
 * Track Scrolling
 * @param scrollDirection Enumeration scroll type
 * @param distance Distance scrolling
 */
-(void)trackScroll:(ScrollDirection)scrollDirection distance:(NSInteger)distance;

/**
 * Track when enter to screen
 * @param customProperties Custom NSDictionary property
 */
-(void)trackEnterViewPort:(NSDictionary<NSString*,id> *)customProperties;

/**
 * Track when exit from screen
 * @param customProperties Custom NSDictionary property
 */
-(void)trackExitViewPort:(NSDictionary<NSString*,id> *)customProperties;

/**
 * Track position change
 * @param location CLLocation object from GPS
 */
-(void)trackPositionChange:(CLLocation *)location;

/**
 * Track when opening application
 * @param uri URI Tracking
 * @param customProperties Custom NSDictionary property
 */
-(void)trackStart:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties;
-(void)trackStart:(NSString *)uri;

/**
 * Track when exit from application
 */
-(void)trackEnd;

/**
 * Track custom event
 * @param eventType Type event type property
  * @param customProperties Custom JSON Data for event
 */
-(void)trackCustomEvent:(NSString *)eventType customProperties:(NSDictionary<NSString*,id> *)customProperties;

/**
 * Track personalise JSON respond from REST
 * @param data Personalise JSON Data for event
 */
-(void)trackPersonalise:(NSDictionary<NSString*,id> *)data;

/**
 * Track convert JSON respond from REST
 * @param data Convert JSON Data for event
 */
-(void)trackConvert:(NSDictionary<NSString*,id> *)data;

// Getters
-(NSString *)getDID;
-(NSString *)getSID;

@end
