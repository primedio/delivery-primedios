#!/usr/bin/env bash
if [ -z $1 ]; then echo "you must provide a version number"; exit 1; fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


gsed -i 's/s.version          = ".*"/s.version          = "'$1'"/g' $DIR/PrimedIO.podspec
gsed -i 's/@"sdkVersion":@".*"/@"sdkVersion":@"'$1'"/g' $DIR/PrimedIO/Classes/PrimedTracker.m
