# DEPRECIATION NOTE
This PrimedIO SDK will be deprecated in favor of a Swift based project [here](https://gitlab.com/primedio/delivery-primedios-swift). This project will only receive bugfixes and security updates.

# Installation with CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Objective-C, which automates and simplifies the process of using 3rd-party libraries. You can install it with the following command:

```bash
$ gem install cocoapods
```


# Installation

Add PrimedIO pod to `Podfile`
```ruby
pod 'PrimedIO', '~> <VERSION>'
```

Then, run the following command:

```ruby
pod install
```

## If using Swift you need to add Bridge Header
### Add new header file
Click on File -> New -> File… and select “Header File” in the “Source” tab.

### Name Header File
Name this file Bridging-Header.h

###  Locate Header File in Build Settings
Open your project Build Settings and search for “Bridging”. Edit the key “Objective-C Bridging Header” to project_name/Bridging-Header.h

### Import pod
You are now ready to add your imports into your Bridging-Header.h file for the pods you want to use.
```objective-c
#ifndef Bridging_Header_h
#define Bridging_Header_h

#import <PrimedIO/Primed.h>
#import <PrimedIO/PrimedTracker.h>

#endif /* Bridging_Header_h */
```

# Usage Primed Tracker
## Initialisation
```objective-c
[PrimedTracker.sharedInstance initWithPublicKey:@"somekey" secretKey:@"somesecretkeywithalotofchars" connectionString:@"API_URL_GO_HERE" trackingConnectionString:@"WEBSOCKET_URL_GO_HERE" heartbeatInterval:10 eventQueueRetryInterval:10];
```
**customBasicProperties**
Add custom basic properties.
```objective-c
PrimedTracker.sharedInstance.customBasicProperties = @{@"name":@"Joe",@"age":@"32"};
```

## Track events
**trackClick**
Sends a click event to Primed.
```objective-c
-(void)trackClick:(NSInteger)x y:(NSInteger)y interactionType:(InteractionType)interactionType;
-(void)trackClick:(NSInteger)x y:(NSInteger)y;
```

**trackView**
Sends a view event to Primed. The event requires at least a unique identifier (`uri`) for the page, or view that the user is viewing. The `uri` can be a typical web url (e.g. `http://example.com/articles/1`), or it can indicate a hierarchical view identifer (e.g. `app.views.settings`). Additionally, the call expects a `customProperties` `NSDictionary`, which holds user-defined key-value properties. The keys are always `NSString` and values can be any (boxed) primitive type (`NSInteger`, `NSFloat`, `NSString`, etc.).
```objective-c
-(void)trackView:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties;
-(void)trackView:(NSString *)uri;
```

**trackScroll**
Sends a scroll event to Primed. The event requires a `ScrollDirection` argument, which indicates whether the user scrolled up, down, left or right and a `distance` in pixels.
```objective-c
-(void)trackScroll:(ScrollDirection)scrollDirection distance:(NSInteger)distance;
```

**trackEnterViewport**
Sends an enterViewPort event to Primed. This event is generally called whenever an items appears into view for the user. the call expects a `customProperties` NSDictionary, which holds user-defined key-value properties. The keys are always `NSString` and values can be any (boxed) primitive type (`NSInteger`, `NSFloat`, `NSString`, etc.).
```objective-c
-(void)trackEnterViewPort:customProperties:(NSDictionary<NSString*,id> *)customProperties;
```

**trackExitViewport**
Sends an exitViewPort event to Primed. This event is generally called whenever an items disappears from view for the user. the call expects a `customProperties` NSDictionary, which holds user-defined key-value properties. The keys are always `NSString` and values can be any (boxed) primitive type (`NSInteger`, `NSFloat`, `NSString`, etc.).
```objective-c
-(void)trackExitViewPort:(NSDictionary<NSString*,id> *)customProperties;
```

**trackPositionChange**
Sends a positionChange event to Primed.
```objective-c
-(void)trackPositionChange:(CLLocation *)location;
```

**trackStart**
Sends a start event to Primed. The event requires at least a unique identifier (`uri`) for the page, or view that the user entered the application on (usually the start or homepage). The `uri` can be a typical web url (e.g. `http://example.com/articles/1`), or it can indicate a hierarchical view identifer (e.g. `app.views.settings`). Additionally, the call expects a `customProperties` NSDictionary, which holds user-defined key-value properties. The keys are always `NSString` and values can be any (boxed) primitive type (`NSInteger`, `NSFloat`, `NSString`, etc.).
```objective-c
-(void)trackStart:(NSString *)uri customProperties:(NSDictionary<NSString*,id> *)customProperties;
-(void)trackStart:(NSString *)uri;
```

**trackEnd**
Sends a end event to Primed. The event expects no arguments.
```objective-c
-(void)trackEnd;
```

**trackCustomEvent**
User defined event. For example defining a custom `VIDEOSTART` event, which takes one custom property (itemId), looks as follows:
```objective-c
[PrimedTracker.sharedInstance trackCustomEvent:@"VIDEOSTART" customProperties:@{@"itemId":@"32"}];
```

```objective-c
-(void)trackCustomEvent:(NSString *)eventType customProperties:(NSDictionary<NSString*,id> *)data;

```

# Usage Primed calls
## Initialisation
```objective-c
[Primed.sharedInstance initWithPublicKey:@"somekey" secretKey:@"somesecretkeywithalotofchars" connectionString:@"API_URL_GO_HERE"]
```

## Convert (ASYNC):
Upon successful personalisation, a list of results will be returned. Each result will contain a variable payload: the idea here is that PrimedIO is generic and supports freeform `Targets`, which can be item ID's (usually used in lookups after the personalise call), URL's, Boolean values, and any combination of these. Additionally, each result will contain a unique RUUID (Result UUID), randomly generated for this particular call and `Target`. It is used to track the conversion of this particular outcome, which is used to identify which of the A/B variants performed best. Conversion is also generic, but generally we can associate this with clicking a particular outcome. In order for PrimedIO to register this feedback, another call needs to be made upon conversion. This in turn allows the system to evaluate the performance (as CTR) of the underlying Model (or blend).

```objective-c
[Primed.sharedInstance convert:@"RUUID_GO_HERE"];
    
NSDictionary *dictData = @{@"device": @"iphone",
                            @"userid": @"someuserid"
                        };
[Primed.sharedInstance convert:@"RUUID_GO_HERE" data:dictData];
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| ruuid | NSString | Yes | ruuid for which to register conversion | `"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"` |
| data | NSDictionary<NSString \*, NSObject \*> | No | freeform data payload | `{ heartbeat: 0.1 }` |

## Personalise (ASYNC):
This call obtains predictions for a given campaign and calls the callback function with the result. Personalisation requires at least a campaign key (NB, this is not the campaign name), e.g. `frontpage.recommendations`. 

```objective-c
NSDictionary *dictSignals = @{@"device": @"iphone",
                             @"userid": @"someuserid"
                            };
    
[Primed.sharedInstance personalise:@"frontpage.article.bottom" signals:dictSignals limit:3 success:^(NSDictionary *response) {    
    //Handle response
    NSLog(@"personalise: %@",response);
} failed:^(NSString *message) {
    //Handle message
    NSLog(@"personalise error: %@",message);
}];
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| campaign | NSString | Yes | campaign key for which personalisation is retrieved | `frontpage.recommendations` |
| signals | NSDictionary<NSString \*, NSString \*> | No (defaults to `{}`) | key, value pairs of signals (itemId currently being viewed, deviceId, location, etc.) | `{itemId: '1234', userId: 'xyz'}` |
| limit | NSInteger | No (defaults to `5`) | number of desired results | `10` |
| abvariantLabel | NSString | No (defaults to `WRANDOM` assignment) | specify A/B variant for which to retrieve personalisation | `__CONTROL__` |
| success:\^(NSDictionary *response) | *callback function* | Yes | called when results returned succesfully |  |
| failed:\^(NSString *message) | *callback function* | Yes | called in case of failure |  |


EXAMPLE RETURN VALUE:
```
{
	"campaign": {
        "key": "dummy.frontpage.recommendations"
    },
    "experiment": {
        "name": "myfirstexperiment",
        "salt": "sadfasdf",
        "abmembership_strategy": "WRANDOM",
        "abselectors": [],
        "matched_abselector": {
            "null": "IU8LOR2JL5LS3ZVN"
        },
        "abvariant": {
            "label": "A",
            "dithering": false,
            "recency": false
        }
    },
	"query_latency_ms": 42.66,
    "guuid": "554fe95a-214e-4d82-921a-e202eacf373b",
	'results': [
		{
			"target": {
                "uid": "50371aafd2f545809732877dc0cfb86e",
                "key": "8ba254d9-78d4-4b11-9e04-692bacda9c56",
                "value": {
                    "title": "2",
                    "url": "http://fields.com/2"
                }
            },
            "fscore": 0.7789,
            "components": [
                {
                    "model_uid": "b8071e37fe8547a5825d10ab17a05932",
                    "weight": 0.4,
                    "signal_uid": "d4358441be124902bb60972a6c555f80",
                    "cscore": 0.8150115180517363
                },
                {
                    "model_uid": "dc883e27a5a44e3ab7027f3619b3d726",
                    "weight": 0.6,
                    "signal_uid": "0d3afe0bc4b84200a403ad0edf9ffcc7",
                    "cscore": 0.7547711855338738
                }
            ],
            "recency_factor": 1.0,
            "ruuid": "ec12da8c-a045-4dfb-8618-d9d4f424ce24"

		}
	]
}
```
