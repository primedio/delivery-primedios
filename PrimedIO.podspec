#
# Be sure to run `pod lib lint PrimedIO.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'PrimedIO'
    s.version          = "0.0.6"
    s.summary          = 'IOS SDK for retrieving predictions from the Primed Backend, as well as track behavior'
    s.description      = <<-DESC
    IOS SDK for retrieving predictions from the Primed Backend, as well as track behavior.
    DESC
    
    s.homepage         = 'https://gitlab.com/primedio/delivery-primedios'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'PrimedIO' => 'info@primed.io' }
    s.source           = { :git => 'https://gitlab.com/primedio/delivery-primedios.git', :tag => s.version.to_s }
    s.ios.deployment_target = '9.0'
    s.source_files = 'PrimedIO/Classes/**/*'
    s.swift_version = '4.0'
    
    s.dependency "Socket.IO-Client-Swift", "~> 13.3.0"
end
