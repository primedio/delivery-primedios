//
//  PIOViewController.m
//  PrimedIO
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

#import "PIOViewController.h"
#import "PIOViewControllerDemo_Primed.h"
#import "PIOViewControllerDemo_PrimedTracker.h"

@interface PIOViewController ()

@end

@implementation PIOViewController

#pragma mark - IBActions
-(IBAction)onBtnOpenPrimed:(UIButton *)sender{
    PIOViewControllerDemo_Primed *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Primed"];
    [self presentViewController:controller animated:YES completion:nil];
}
-(IBAction)onBtnOpenPrimedTracker:(UIButton *)sender{
    PIOViewControllerDemo_PrimedTracker *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PrimedTracker"];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - UIViewDelegates
-(void)viewDidLoad{
    [super viewDidLoad];
}
-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
