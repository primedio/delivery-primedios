//
//  PIOViewControllerDemo_Primed.h
//  PrimedIO
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIOViewControllerDemo_Primed : UIViewController{
    IBOutlet UITextView *txtConsole;
}

@end
