//
//  PIOViewControllerDemo_Primed.m
//  PrimedIO
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

#import "PIOViewControllerDemo_Primed.h"
#import <PrimedIO/Primed.h>

@interface PIOViewControllerDemo_Primed ()

@end

@implementation PIOViewControllerDemo_Primed

#pragma mark - IBActions
-(IBAction)onBtnConvert:(UIButton *)sender{
    // Make conversion
    //    [Primed.sharedInstance convert:@"43b88f01-e3e7-4f8d-a0d2-bcc1f836650a"];
    
    // Make conversion with data
    NSDictionary *dictData = @{@"device": @"iphone",
                               @"userid": @"someuserid"
                               };
    [Primed.sharedInstance convert:@"43b88f01-e3e7-4f8d-a0d2-bcc1f836650a" data:dictData];
}
-(IBAction)onBtnPersonalise:(UIButton *)sender{
    NSDictionary *dictSignals = @{@"device": @"iphone",
                                  @"userid": @"someuserid"
                                  };
    
//    [Primed.sharedInstance personalise:@"frontpage.recommendations" limit:3 abvariantLabel:@"A" success:^(NSDictionary<NSString *,id> *response) {
//        //Handle response
//        NSLog(@"personalise: %@",response);
//        txtConsole.text = [NSString stringWithFormat:@"personalise: %@",response];
//    } failed:^(NSString *message) {
//        //Handle message
//        NSLog(@"personalise error: %@",message);
//        txtConsole.text = [NSString stringWithFormat:@"personalise error: %@",message];
//    }];
    
    [Primed.sharedInstance personalise:@"frontpage.recommendations" signals:dictSignals limit:3 abvariantLabel:@"A" success:^(NSDictionary *response) {
        //Handle response
        NSLog(@"personalise: %@",response);
        txtConsole.text = [NSString stringWithFormat:@"personalise: %@",response];
    } failed:^(NSString *message) {
        //Handle message
        NSLog(@"personalise error: %@",message);
        txtConsole.text = [NSString stringWithFormat:@"personalise error: %@",message];
    }];
}
-(IBAction)onBtnCheckHealth:(UIButton *)sender{
    [Primed.sharedInstance health:^(NSDictionary *response) {
        NSLog(@"health: %@",response);
        txtConsole.text = [NSString stringWithFormat:@"health: %@",response];
    } failed:^(NSString *message) {
        NSLog(@"health error: %@",message);
        txtConsole.text = [NSString stringWithFormat:@"health error: %@",message];
    }];
}

#pragma mark - UIViewDelegates
-(void)viewDidLoad {
    [super viewDidLoad];
    
    // Enable debug
     Primed.sharedInstance.debug = YES;
    
    // Create Primed instance
    [Primed.sharedInstance initWithPublicKey:@"mypubkey" secretKey:@"mysecretkey" connectionString:@"http://localhost:5000"];
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
