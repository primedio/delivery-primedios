//
//  PIOViewControllerDemo_PrimedTracker.m
//  PrimedIO
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

#import "PIOViewControllerDemo_PrimedTracker.h"
#import "PIOAppDelegate.h"
#import <PrimedIO/PrimedTracker.h>

@interface PIOViewControllerDemo_PrimedTracker (){
    CGFloat lastContentOffset;
}

@end

@implementation PIOViewControllerDemo_PrimedTracker

-(void)trackLocation{
    PIOAppDelegate *appDelegate = (PIOAppDelegate *)[[UIApplication sharedApplication] delegate];
    [PrimedTracker.sharedInstance trackPositionChange:appDelegate.locationManager.location];
}

#pragma mark - IBActions
-(IBAction)onBtnTrackView:(UIButton *)sender{
//    [PrimedTracker.sharedInstance trackView:@"Test View Example"];
    [PrimedTracker.sharedInstance trackView:@"Test View Example" customProperties:@{@"id":@"33"}];
}
-(IBAction)onBtnTrackEnterViewPort:(UIButton *)sender{
    [PrimedTracker.sharedInstance trackEnterViewPort:@{@"name":@"Screen 1"}];
}
-(IBAction)onBtnTrackExitViewPort:(UIButton *)sender{
    [PrimedTracker.sharedInstance trackExitViewPort:@{@"name":@"Screen 1"}];
}
-(IBAction)onBtnTrackCustomEvent:(UIButton *)sender{
    [PrimedTracker.sharedInstance trackCustomEvent:@"CustomEvenet" customProperties:@{@"id":@"33"}];
}

#pragma mark - UIScroll for TextView
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset.x;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (lastContentOffset < scrollView.contentOffset.y) {
        double distance = scrollView.contentOffset.y - lastContentOffset;
        [PrimedTracker.sharedInstance trackScroll:ScrollDirection_DOWN distance:distance];
    }
    else {
        double distance = lastContentOffset - scrollView.contentOffset.y;
        [PrimedTracker.sharedInstance trackScroll:ScrollDirection_UP distance:distance];
    }
}

#pragma mark - UIViewDelegates
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *aTouch = [touches anyObject];
    CGPoint point = [aTouch locationInView:self.view];
    
    //[PrimedTracker.sharedInstance trackClick:point.x y:point.y];
    [PrimedTracker.sharedInstance trackClick:point.x y:point.y interactionType:InteractionType_LEFT];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    // Create PrimedTracker instance
    [PrimedTracker.sharedInstance initWithPublicKey:@"mypubkey" secretKey:@"mysecretkey" connectionString:@"http://localhost:5000" trackingConnectionString:@"http://localhost:5001" heartbeatInterval:10 eventQueueRetryInterval:10];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    
    if (@available(iOS 10.0, *)) {
        [NSTimer scheduledTimerWithTimeInterval:10 repeats:YES block:^(NSTimer * _Nonnull timer) {
            [self trackLocation];
        }];
    } else {
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(trackLocation) userInfo:nil repeats:YES];
    }
    
    
    
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-(void)appWillResignActive:(NSNotification*)note
//{
//    NSLog(@"appWillResignActive");
//}
//-(void)appWillTerminate:(NSNotification*)note
//{
//    NSLog(@"appWillTerminate");
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
//
//}

@end
