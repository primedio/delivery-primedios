//
//  main.m
//  PrimedIO
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

@import UIKit;
#import "PIOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PIOAppDelegate class]));
    }
}
