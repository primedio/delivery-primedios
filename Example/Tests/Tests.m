//
//  PrimedIOTests.m
//  PrimedIOTests
//
//  Created by PrimedIO on 07/10/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

@import XCTest;

@import PrimedIO;

@interface Tests : XCTestCase

@end

@implementation Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:@"2014-04-03"];
    
    NSString *nonce = [NSString stringWithFormat:@"%.f",date.timeIntervalSince1970];
    NSString *public_key = @"this_is_fake";
    NSString *private_key = @"this_is_also_fake";
    
    NSString *prepSignature = [NSString stringWithFormat:@"%@%@%@",public_key,private_key,nonce];
    NSString *sha512Signature = [PrimedIO hmac:prepSignature];
    
    NSString *sha512Expect = @"71c500ca5a6b22c805da2885c4b9ea795b0a6b57369917b9c7814f6d679aeb40c1cca6d965f9a0cf621eb0d02997ea3c5bb5e908c61b257e60f0442bf37eb3e5";
    
    XCTAssertEqualObjects(sha512Signature,sha512Expect,@"Signature HMAC Not same");
}

@end

